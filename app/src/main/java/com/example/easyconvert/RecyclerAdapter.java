package com.example.easyconvert;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private List<String> textList = new ArrayList<>();
    private Map<String, Integer> mapList = new HashMap<>();
    private Map<String, Float> valueList = new HashMap<>();
    private String base;


    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        holder.setTextView( "1 " + base + " = " + valueList.get(textList.get(position)) + " " + textList.get(position));
        holder.setImageView(mapList.get(textList.get(position)));

    }

    @Override
    public int getItemCount() {
        return textList.size();
    }

    public  void addData(List<String> text, Map<String, Integer> map, Map<String, Float> value , String base){
        this.mapList.clear();
        this.mapList.putAll(map);
        this.textList.clear();
        this.textList.addAll(text);
        this.valueList.clear();
        this.valueList.putAll(value);
        this.base = base;
        notifyDataSetChanged();
    }
}
