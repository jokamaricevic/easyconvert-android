package com.example.easyconvert;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<SpinnerItem> {

    public SpinnerAdapter(Context context, ArrayList<SpinnerItem> spinnerItems){
        super(context, 0, spinnerItems);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent){
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.currency_spinner, parent, false );
        }

        ImageView imageViewFlag = convertView.findViewById(R.id.flag);
        TextView textViewCurrency = convertView.findViewById(R.id.currency);

        SpinnerItem spinnerItem = getItem(position);

        if (spinnerItem != null) {
            imageViewFlag.setImageResource(spinnerItem.getmFlagImage());
            textViewCurrency.setText(spinnerItem.getmCurrency());
        }
        return convertView;

    }






}
