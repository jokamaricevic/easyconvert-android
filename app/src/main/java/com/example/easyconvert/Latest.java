package com.example.easyconvert;

public class Latest {
    private String base;
    private String date;
    private Rates rates;

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public Rates getRates() {
        return rates;
    }
}
