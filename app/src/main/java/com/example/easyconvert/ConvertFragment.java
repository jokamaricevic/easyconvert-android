package com.example.easyconvert;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConvertFragment extends Fragment {

    private ArrayList<SpinnerItem> spinnerList;
    private String from;
    private String to;

    private View view;
    private EditText amount;
    private TextView result;
    private TextView result2;
    private TextView resultMain;
    private TextView label;

    public static ConvertFragment newInstance(){
        return new ConvertFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.convert_fragment,container, false);

        initViews();
        initList();
        initSpinner();





        return  view;
    }

    private void setResults(Map<String,Float> rates){

        label.setText("");
        result.setText("1 " + from + " = " + rates.get(to).toString() + " " + to);
        result2.setText("1 " + to + " = " + (1/rates.get(to)) + " " + from);
        resultMain.setText(amount.getText().toString() + " " + from + " = " + rates.get(to)*Float.valueOf(amount.getText().toString()) + " " + to );


    }

    private void initViews(){
        amount = view.findViewById(R.id.editText);
        result = view.findViewById(R.id.result);
        result2 = view.findViewById(R.id.result2);
        resultMain = view.findViewById(R.id.resultMain);
        label = view.findViewById(R.id.Label);
        label.setText("Currency\nConverter");


        Button convert = view.findViewById(R.id.button);
        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(amount.getText().toString().isEmpty()){
                    Toast.makeText(view.getContext(), "Enter amount first!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                getRates(from);
            }
        });

        Button clear = view.findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText("");
                result.setText("");
                result2.setText("");
                resultMain.setText("");
                label.setText("Currency\nConverter");
            }
        });
        amount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    ConvertFragment.this.getRates(from);
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

    }

    private void initList(){
        spinnerList = new ArrayList<>();
        spinnerList.add(new SpinnerItem("AUD", R.drawable.aud));
        spinnerList.add(new SpinnerItem("BGN", R.drawable.bgn));
        spinnerList.add(new SpinnerItem("BRL", R.drawable.brl));
        spinnerList.add(new SpinnerItem("CAD", R.drawable.cad));
        spinnerList.add(new SpinnerItem("CHF", R.drawable.chf));
        spinnerList.add(new SpinnerItem("CNY", R.drawable.cny));
        spinnerList.add(new SpinnerItem("CZK", R.drawable.czk));
        spinnerList.add(new SpinnerItem("DKK", R.drawable.dkk));
        spinnerList.add(new SpinnerItem("EUR", R.drawable.eur));
        spinnerList.add(new SpinnerItem("GBP", R.drawable.gbp));
        spinnerList.add(new SpinnerItem("HKD", R.drawable.hkd));
        spinnerList.add(new SpinnerItem("HRK", R.drawable.hrk));
        spinnerList.add(new SpinnerItem("HUF", R.drawable.huf));
        spinnerList.add(new SpinnerItem("IDR", R.drawable.idr));
        spinnerList.add(new SpinnerItem("ILS", R.drawable.ils));
        spinnerList.add(new SpinnerItem("INR", R.drawable.inr));
        spinnerList.add(new SpinnerItem("ISK", R.drawable.isk));
        spinnerList.add(new SpinnerItem("JPY", R.drawable.jpy));
        spinnerList.add(new SpinnerItem("KRW", R.drawable.krw));
        spinnerList.add(new SpinnerItem("MXN", R.drawable.mxn));
        spinnerList.add(new SpinnerItem("MYR", R.drawable.myr));
        spinnerList.add(new SpinnerItem("NOK", R.drawable.nok));
        spinnerList.add(new SpinnerItem("NZD", R.drawable.nzd));
        spinnerList.add(new SpinnerItem("PHP", R.drawable.php));
        spinnerList.add(new SpinnerItem("PLN", R.drawable.pln));
        spinnerList.add(new SpinnerItem("RON", R.drawable.ron));
        spinnerList.add(new SpinnerItem("RUB", R.drawable.rub));
        spinnerList.add(new SpinnerItem("SEK", R.drawable.sek));
        spinnerList.add(new SpinnerItem("SGD", R.drawable.sgd));
        spinnerList.add(new SpinnerItem("THB", R.drawable.thb));
        spinnerList.add(new SpinnerItem("TRY", R.drawable.trj));
        spinnerList.add(new SpinnerItem("USD", R.drawable.usd));
        spinnerList.add(new SpinnerItem("ZAR", R.drawable.zar));

    }

    private void initSpinner(){
        Spinner spinner_form;
        Spinner spinner_to;
        spinner_form = view.findViewById(R.id.spinner_from);
        spinner_to = view.findViewById(R.id.spinner_to);


        SpinnerAdapter adapter = new SpinnerAdapter(view.getContext(), spinnerList);

        spinner_form.setAdapter(adapter);
        spinner_form.setSelection(31);


        spinner_form.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItem spinnerItem = (SpinnerItem) parent.getItemAtPosition(position);
                from = spinnerItem.getmCurrency();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
        spinner_form.setSelected(true);

        spinner_to.setAdapter(adapter);
        spinner_to.setSelection(8);
        spinner_to.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItem spinnerItem = (SpinnerItem) parent.getItemAtPosition(position);
                to = spinnerItem.getmCurrency();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getRates(String base){

        final Map<String,Float> rates = new HashMap<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.exchangeratesapi.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderAPI jsonPlaceHolderAPI = retrofit.create(JsonPlaceHolderAPI.class);

        Call<Latest> call = jsonPlaceHolderAPI.getLatest(base);

        call.enqueue(new Callback<Latest>() {
            @Override
            public void onResponse(@SuppressWarnings("NullableProblems") Call<Latest> call, Response<Latest> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(view.getContext(), response.code(), Toast.LENGTH_SHORT).show();
                }

                Latest latest = response.body();

                rates.put("CAD", latest.getRates().getCAD());
                rates.put("HKD", latest.getRates().getHKD());
                rates.put("ISK", latest.getRates().getISK());
                rates.put("PHP", latest.getRates().getPHP());
                rates.put("DKK", latest.getRates().getDKK());
                rates.put("HUF", latest.getRates().getHUF());
                rates.put("CZK", latest.getRates().getCZK());
                rates.put("GBP", latest.getRates().getGBP());
                rates.put("RON", latest.getRates().getRON());
                rates.put("SEK", latest.getRates().getSEK());
                rates.put("IDR", latest.getRates().getIDR());
                rates.put("INR", latest.getRates().getINR());
                rates.put("BRL", latest.getRates().getBRL());
                rates.put("RUB", latest.getRates().getRUB());
                rates.put("HRK", latest.getRates().getHRK());
                rates.put("JPY", latest.getRates().getJPY());
                rates.put("THB", latest.getRates().getTHB());
                rates.put("CHF", latest.getRates().getCHF());
                rates.put("EUR", latest.getRates().getEUR());
                rates.put("MYR", latest.getRates().getMYR());
                rates.put("BGN", latest.getRates().getBGN());
                rates.put("TRY", latest.getRates().getTRY());
                rates.put("CNY", latest.getRates().getCNY());
                rates.put("NOK", latest.getRates().getNOK());
                rates.put("NZD", latest.getRates().getNZD());
                rates.put("ZAR", latest.getRates().getZAR());
                rates.put("USD", latest.getRates().getUSD());
                rates.put("MXN", latest.getRates().getMXN());
                rates.put("SGD", latest.getRates().getSGD());
                rates.put("AUD", latest.getRates().getAUD());
                rates.put("ILS", latest.getRates().getILS());
                rates.put("KRW", latest.getRates().getKRW());
                rates.put("PLN", latest.getRates().getPLN());

                setResults(rates);


            }

            @Override
            public void onFailure(@SuppressWarnings("NullableProblems") Call<Latest> call, @SuppressWarnings("NullableProblems") Throwable t) {
                Toast.makeText(view.getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
