package com.example.easyconvert;

class Rates {
    private Float CAD;
    private Float HKD;
    private Float ISK;
    private Float PHP;
    private Float DKK;
    private Float HUF;
    private Float CZK;
    private Float GBP;
    private Float RON;
    private Float SEK;
    private Float IDR;
    private Float INR;
    private Float BRL;
    private Float RUB;
    private Float HRK;
    private Float JPY;
    private Float THB;
    private Float CHF;
    private Float EUR;
    private Float MYR;
    private Float BGN;
    private Float TRY;
    private Float CNY;
    private Float NOK;
    private Float NZD;
    private Float ZAR;
    private Float USD;
    private Float MXN;
    private Float SGD;
    private Float AUD;
    private Float ILS;
    private Float KRW;
    private Float PLN;

    public Float getCAD() {
        return CAD;
    }

    public Float getHKD() {
        return HKD;
    }

    public Float getISK() {
        return ISK;
    }

    public Float getPHP() {
        return PHP;
    }

    public Float getDKK() {
        return DKK;
    }

    public Float getHUF() {
        return HUF;
    }

    public Float getCZK() {
        return CZK;
    }

    public Float getGBP() {
        return GBP;
    }

    public Float getRON() {
        return RON;
    }

    public Float getSEK() {
        return SEK;
    }

    public Float getIDR() {
        return IDR;
    }

    public Float getINR() {
        return INR;
    }

    public Float getBRL() {
        return BRL;
    }

    public Float getRUB() {
        return RUB;
    }

    public Float getHRK() {
        return HRK;
    }

    public Float getJPY() {
        return JPY;
    }

    public Float getTHB() {
        return THB;
    }

    public Float getCHF() {
        return CHF;
    }

    public Float getEUR() {
        return EUR;
    }

    public Float getMYR() {
        return MYR;
    }

    public Float getBGN() {
        return BGN;
    }

    public Float getTRY() {
        return TRY;
    }

    public Float getCNY() {
        return CNY;
    }

    public Float getNOK() {
        return NOK;
    }

    public Float getNZD() {
        return NZD;
    }

    public Float getZAR() {
        return ZAR;
    }

    public Float getUSD() {
        return USD;
    }

    public Float getMXN() {
        return MXN;
    }

    public Float getSGD() {
        return SGD;
    }

    public Float getAUD() {
        return AUD;
    }

    public Float getILS() {
        return ILS;
    }

    public Float getKRW() {
        return KRW;
    }

    public Float getPLN() {
        return PLN;
    }
}



