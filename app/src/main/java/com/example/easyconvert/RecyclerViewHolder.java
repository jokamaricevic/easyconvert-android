package com.example.easyconvert;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    private TextView mTextView;
    private ImageView mImageView;

    public RecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        mTextView = itemView.findViewById(R.id.display);
        mImageView = itemView.findViewById((R.id.flag_image));
    }

    public void setTextView(String text) {
        mTextView.setText(text);
    }

    public void setImageView(Integer image) {
        mImageView.setImageResource(image);
    }
}
