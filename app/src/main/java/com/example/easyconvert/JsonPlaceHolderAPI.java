package com.example.easyconvert;



import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceHolderAPI {

    @GET("latest?")
    Call<Latest> getLatest(@Query("base") String base);
}
