package com.example.easyconvert;

public class SpinnerItem {

    private String mCurrency;
    private int mFlagImage;

    public SpinnerItem(String mCurrency, int mFlagImage) {
        this.mCurrency = mCurrency;
        this.mFlagImage = mFlagImage;
    }


    public String getmCurrency() {
        return mCurrency;
    }

    public int getmFlagImage() {
        return mFlagImage;
    }
}
