package com.example.easyconvert;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OverviewFragment extends Fragment {

    private View view;
    private ArrayList<SpinnerItem> spinnerList;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;



    public static OverviewFragment newInstance(){
        return new OverviewFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.overview_fragment,container, false);

        initList();
        initSpinner();
        initViews();
        getRates("EUR");






        return view;
    }

    private void setRecyclerData(Map<String,Float> value , String base){
        Map<String,Integer> map = new HashMap<>();
        map.put("CAD", R.drawable.cad);
        map.put("HKD", R.drawable.hkd);
        map.put("EUR", R.drawable.eur);
        map.put("ISK", R.drawable.isk);
        map.put("PHP", R.drawable.php);
        map.put("DKK", R.drawable.dkk);
        map.put("HUF", R.drawable.huf);
        map.put("CZK", R.drawable.czk);
        map.put("AUD", R.drawable.aud);
        map.put("RON", R.drawable.ron);
        map.put("SEK", R.drawable.sek);
        map.put("IDR", R.drawable.idr);
        map.put("INR", R.drawable.inr);
        map.put("BRL", R.drawable.brl);
        map.put("RUB", R.drawable.rub);
        map.put("HRK", R.drawable.hrk);
        map.put("JPY", R.drawable.jpy);
        map.put("THB", R.drawable.thb);
        map.put("CHF", R.drawable.chf);
        map.put("SGD", R.drawable.sgd);
        map.put("PLN", R.drawable.pln);
        map.put("BGN", R.drawable.bgn);
        map.put("TRY", R.drawable.trj);
        map.put("CNY", R.drawable.cny);
        map.put("NOK", R.drawable.nok);
        map.put("NZD", R.drawable.nzd);
        map.put("ZAR", R.drawable.zar);
        map.put("USD", R.drawable.usd);
        map.put("MXN", R.drawable.mxn);
        map.put("ILS", R.drawable.ils);
        map.put("GBP", R.drawable.gbp);
        map.put("KRW", R.drawable.krw);
        map.put("MYR", R.drawable.myr);

        List<String> data = new ArrayList<>(map.keySet());

        Collections.sort(data);

        adapter.addData(data, map, value, base);
    }


    private void initViews(){
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);

    }

    private void initList(){
        spinnerList = new ArrayList<>();
        spinnerList.add(new SpinnerItem("AUD", R.drawable.aud));
        spinnerList.add(new SpinnerItem("BGN", R.drawable.bgn));
        spinnerList.add(new SpinnerItem("BRL", R.drawable.brl));
        spinnerList.add(new SpinnerItem("CAD", R.drawable.cad));
        spinnerList.add(new SpinnerItem("CHF", R.drawable.chf));
        spinnerList.add(new SpinnerItem("CNY", R.drawable.cny));
        spinnerList.add(new SpinnerItem("CZK", R.drawable.czk));
        spinnerList.add(new SpinnerItem("DKK", R.drawable.dkk));
        spinnerList.add(new SpinnerItem("EUR", R.drawable.eur));
        spinnerList.add(new SpinnerItem("GBP", R.drawable.gbp));
        spinnerList.add(new SpinnerItem("HKD", R.drawable.hkd));
        spinnerList.add(new SpinnerItem("HRK", R.drawable.hrk));
        spinnerList.add(new SpinnerItem("HUF", R.drawable.huf));
        spinnerList.add(new SpinnerItem("IDR", R.drawable.idr));
        spinnerList.add(new SpinnerItem("ILS", R.drawable.ils));
        spinnerList.add(new SpinnerItem("INR", R.drawable.inr));
        spinnerList.add(new SpinnerItem("ISK", R.drawable.isk));
        spinnerList.add(new SpinnerItem("JPY", R.drawable.jpy));
        spinnerList.add(new SpinnerItem("KRW", R.drawable.krw));
        spinnerList.add(new SpinnerItem("MXN", R.drawable.mxn));
        spinnerList.add(new SpinnerItem("MYR", R.drawable.myr));
        spinnerList.add(new SpinnerItem("NOK", R.drawable.nok));
        spinnerList.add(new SpinnerItem("NZD", R.drawable.nzd));
        spinnerList.add(new SpinnerItem("PHP", R.drawable.php));
        spinnerList.add(new SpinnerItem("PLN", R.drawable.pln));
        spinnerList.add(new SpinnerItem("RON", R.drawable.ron));
        spinnerList.add(new SpinnerItem("RUB", R.drawable.rub));
        spinnerList.add(new SpinnerItem("SEK", R.drawable.sek));
        spinnerList.add(new SpinnerItem("SGD", R.drawable.sgd));
        spinnerList.add(new SpinnerItem("THB", R.drawable.thb));
        spinnerList.add(new SpinnerItem("TRY", R.drawable.trj));
        spinnerList.add(new SpinnerItem("USD", R.drawable.usd));
        spinnerList.add(new SpinnerItem("ZAR", R.drawable.zar));

    }

    private void initSpinner(){
        Spinner spinner;
        spinner = view.findViewById(R.id.spinner_overview);

        SpinnerAdapter adapter = new SpinnerAdapter(view.getContext(), spinnerList);

        spinner.setAdapter(adapter);
        spinner.setSelection(8);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItem spinnerItem = (SpinnerItem) parent.getItemAtPosition(position);
                getRates(spinnerItem.getmCurrency());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getRates(String base){

        final Map<String,Float> rates = new HashMap<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.exchangeratesapi.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderAPI jsonPlaceHolderAPI = retrofit.create(JsonPlaceHolderAPI.class);

        Call<Latest> call = jsonPlaceHolderAPI.getLatest(base);

        call.enqueue(new Callback<Latest>() {
            @Override
            public void onResponse(@SuppressWarnings("NullableProblems") Call<Latest> call, Response<Latest> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(view.getContext(), response.code(), Toast.LENGTH_SHORT).show();
                }

                Latest latest = response.body();

                rates.put("CAD", latest.getRates().getCAD());
                rates.put("HKD", latest.getRates().getHKD());
                rates.put("ISK", latest.getRates().getISK());
                rates.put("PHP", latest.getRates().getPHP());
                rates.put("DKK", latest.getRates().getDKK());
                rates.put("HUF", latest.getRates().getHUF());
                rates.put("CZK", latest.getRates().getCZK());
                rates.put("GBP", latest.getRates().getGBP());
                rates.put("RON", latest.getRates().getRON());
                rates.put("SEK", latest.getRates().getSEK());
                rates.put("IDR", latest.getRates().getIDR());
                rates.put("INR", latest.getRates().getINR());
                rates.put("BRL", latest.getRates().getBRL());
                rates.put("RUB", latest.getRates().getRUB());
                rates.put("HRK", latest.getRates().getHRK());
                rates.put("JPY", latest.getRates().getJPY());
                rates.put("THB", latest.getRates().getTHB());
                rates.put("CHF", latest.getRates().getCHF());
                rates.put("EUR", latest.getRates().getEUR());
                rates.put("MYR", latest.getRates().getMYR());
                rates.put("BGN", latest.getRates().getBGN());
                rates.put("TRY", latest.getRates().getTRY());
                rates.put("CNY", latest.getRates().getCNY());
                rates.put("NOK", latest.getRates().getNOK());
                rates.put("NZD", latest.getRates().getNZD());
                rates.put("ZAR", latest.getRates().getZAR());
                rates.put("USD", latest.getRates().getUSD());
                rates.put("MXN", latest.getRates().getMXN());
                rates.put("SGD", latest.getRates().getSGD());
                rates.put("AUD", latest.getRates().getAUD());
                rates.put("ILS", latest.getRates().getILS());
                rates.put("KRW", latest.getRates().getKRW());
                rates.put("PLN", latest.getRates().getPLN());

                setRecyclerData(rates, base);


            }

            @Override
            public void onFailure(@SuppressWarnings("NullableProblems") Call<Latest> call, @SuppressWarnings("NullableProblems") Throwable t) {
                Toast.makeText(view.getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
